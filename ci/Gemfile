source ENV['GEM_SOURCE'] || "https://rubygems.org"

def location_for(place, fake_version = nil)
  if place =~ /^(git[:@][^#]*)#(.*)/
    [fake_version, { :git => $1, :branch => $2, :require => false }].compact
  elsif place =~ /^file:\/\/(.*)/
    ['>= 0', { :path => File.expand_path($1), :require => false }]
  else
    [place, { :require => false }]
  end
end

group :test do
  gem 'voxpupuli-test', '~> 2.5',   :require => false
  gem 'pathspec', '~> 0',           :require => false
  gem 'puppet-strings', '>= 2.2',   :require => false
  gem 'puppet_metadata', '~> 1.0',  :require => false
  gem 'coveralls',                  :require => false
  gem 'simplecov-console',          :require => false
  gem 'simplecov',                  :require => false
  gem 'rspec_junit_formatter',      :require => false
  gem 'parallel_tests',             :require => false
  gem 'ruby-ldap',                  :require => false
  gem 'net-ldap',                   :require => false
  gem 'rubocop-i18n',               :require => false
end



if facterversion = ENV['FACTER_GEM_VERSION']
  gem 'facter', facterversion.to_s, :require => false, :groups => [:test]
end

ENV['PUPPET_VERSION'].nil? ? puppetversion = '~> 7.0' : puppetversion = ENV['PUPPET_VERSION'].to_s
gem 'puppet', puppetversion, :require => false, :groups => [:test]

# vim: syntax=ruby
